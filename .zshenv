typeset -U path PATH
path=(~/.local/bin $path)
export PATH

export EDITOR=nvim
export BROWSER=firefox
export ZDOTDIR="$HOME"/.config/zsh
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc 
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority 
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc":"$XDG_CONFIG_HOME/gtk-2.0/gtkrc.mine"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
